package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/fajar.basoni/loan_process/cmd"
	"gitlab.com/fajar.basoni/loan_process/common"
	"gitlab.com/fajar.basoni/loan_process/config"
	"gitlab.com/fajar.basoni/loan_process/model"
)

func main() {
	// view path project
	// fmt.Println(os.Getenv("PROJECTPATH"))

	// init database
	config.InitDatabase()
	// import data province
	cmd.ImportData()

	// main
	fmt.Print("$ ")
	reader := bufio.NewReader(os.Stdin)

	input, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("An error occured while reading input. Please try again", err)
		return
	}

	input = strings.TrimSuffix(input, "\n")
	command := strings.Split(input, " ")
	basic := strings.ToLower(command[0])
	switch basic {
	case "create_day_max":
		err := cmd.CreateMaxDay(command[1])
		common.CheckError(err)
		common.PrintLine("Created max request with ", command[1], " requests")
	case "add":
		amountParse, _ := strconv.ParseInt(command[3], 10, 64)
		params := model.Request{
			Id:       cmd.GenerateId(),
			Nik:      command[1],
			FullName: command[2],
			Amount:   amountParse,
			Status:   "",
		}
		result, request := cmd.AddRequest(params)
		if result.Success {
			if request.Status == "ACCEPTED" {
				common.PrintLine("Success: ", request.Id)
			} else {
				common.PrintLine("Request with : ", request.Id, " rejected")
			}
		} else {
			common.PrintLine(result.Message)
		}
	case "status":
		result, request := cmd.GetRequest(command[1])
		if result.Success {
			if request.Id == "" {
				common.PrintLine("Sorry, doesn’t found it")
			} else {
				common.PrintLine("Loan ID ", command[1], " is ", request.Status)
			}
		}
	case "installment":
		tenor, _ := strconv.Atoi(command[2])
		installments, _ := cmd.ListInstallment(command[1], int64(tenor))
		common.PrintLine("Month\t",
			"Due Date\t",
			"Administration Fee\t",
			"Capital\t",
			"Total")

		for i := 0; i < len(installments); i++ {
			common.PrintLine(
				strconv.Itoa(int(installments[i].Month)), "\t",
				installments[i].DueDate, "\t",
				strconv.Itoa(int(installments[i].AdministrationFee)), "\t\t\t",
				strconv.Itoa(int(installments[i].Capital)), "\t",
				strconv.Itoa(int(installments[i].Total)))
		}
	case "find_by_amount_accepted":
		amount, _ := strconv.ParseInt(command[1], 10, 64)
		result, list := cmd.GetRequestByAmount(amount, "ACCEPTED")
		if result.Success {
			if list == "" {
				common.PrintLine("Sorry, doesn’t found it")
			} else {
				common.PrintLine(list)
			}
		}
	case "find_by_amount_rejected":
		amount, _ := strconv.ParseInt(command[1], 10, 64)
		result, list := cmd.GetRequestByAmount(amount, "REJECTED")
		if result.Success {
			if list == "" {
				common.PrintLine("Sorry, doesn’t found it")
			} else {
				common.PrintLine(list)
			}
		}
	case "exit":
		os.Exit(0)
	default:
		common.PrintLine("Command not found !")
	}
	main()
}
