package common

import (
	"fmt"
)

type Result struct {
	Success bool
	Message string
}

// commons
func PrintLine(value ...string) {
	for _, val := range value {
		fmt.Printf("%s", val)
	}
	fmt.Printf("\n")
}

func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}
