# Loan Process

## Setup Project
### Application will need
1. Install Golang : https://golang.org/doc/install
2. Install Sqlite 
```
sudo apt update
```
```
sudo apt install sqlite3
```
```
sqlite --version
```
3. Get Library to connect sqlite driver 
```
go get github.com/mattn/go-sqlite3 
```

## How To Run?
Set Environment `export PROJECTPATH=["PathDirectoryLoanProcessProject"]`
Example : `export PROJECTPATH=$HOME/go/src/gitlab.com/fajar.basoni/loan_process`
Enter to loan_process on your terminal directory, next run command below. 
```
go run main.go
```

### Positive Test Result
```
$ create_day_max 50
Created max request with 50 requests
```
```
$ add 3201021503970008 FajarBasoni 1000000
Success: 04182101
```
```
$ add 3102020109900001 FahriKumala 5000000
Success: 04182102
```
```
$ add 3509024205890020 DindaMantapJiwa 2000000
Success: 04182103
```
```
$ add 3509022205890022 NamaLengkapWanita 5000000
Success: 04182104
```
```
$ status 04182101
Loan ID 04182101 is ACCEPTED
```
```
$ installment 04182101 03
Month   Due Date        Administration Fee      Capital Total
1       18042021        100000                  333333  433333
2       18052021        100000                  333333  433333
3       18062021        100000                  333333  433333
```
```
$ find_by_amount_accepted 5000000
04182102 04182104
```
```
$ find_by_amount_rejected 10000000
Sorry, doesn’t found it
```
```
exit
```

### Negative Test Result
```
$ add 3201021503970008 NamaRejectedMaximalAmount 11000000
Request with : 04182105 rejected
```
```
$ add 3201021503970008 NamaRejectedMinimalAmount 900000          
Request with : 04182106 rejected
```
```
$ add 3201021503970008 NamaRejectedMultipleAmount 5500000
Request with : 04182107 rejected
```
```
$ add 3201021503390008 NamaRejectedMaximalAge 1000000
Request with : 04182108 rejected
```
```
$ add 3601021503970008 NamaRejectedProvince 2000000
Request with : 04182109 rejected
```
```
exit
```

## How To Test?
To try the automatic test, feel free to delete the database file: `data/stored.db`

Test all package : `go test -v -covermode=count -coverprofile=coverage.out ./...`
```
?       gitlab.com/fajar.basoni/loan_process    [no test files]
=== RUN   TestInitDatabaseSuccess
--- PASS: TestInitDatabaseSuccess (0.06s)
=== RUN   TestImportDataInitSuccess
--- PASS: TestImportDataInitSuccess (0.01s)
=== RUN   TestCustomerValidationSuccess
--- PASS: TestCustomerValidationSuccess (0.00s)
=== RUN   TestAllowProvinceSuccess
--- PASS: TestAllowProvinceSuccess (0.00s)
=== RUN   TestListInstallmentSuccess
--- PASS: TestListInstallmentSuccess (0.00s)
=== RUN   TestImportDataSuccess
--- PASS: TestImportDataSuccess (0.00s)
=== RUN   TestAssignProvinceSuccess
--- PASS: TestAssignProvinceSuccess (0.00s)
=== RUN   TestMultipleValidationSuccess
--- PASS: TestMultipleValidationSuccess (0.00s)
=== RUN   TestGenerationIdSuccess
--- PASS: TestGenerationIdSuccess (0.00s)
=== RUN   TestCreateMaxDaySuccess
--- PASS: TestCreateMaxDaySuccess (0.01s)
=== RUN   TestAddRequestSuccess
--- PASS: TestAddRequestSuccess (0.02s)
=== RUN   TestRequestValidationSuccess
--- PASS: TestRequestValidationSuccess (0.00s)
=== RUN   TestGetRequestIdSuccess
--- PASS: TestGetRequestIdSuccess (0.00s)
=== RUN   TestGetRequestByAmountSuccess
--- PASS: TestGetRequestByAmountSuccess (0.00s)
PASS
coverage: 90.5% of statements
ok      gitlab.com/fajar.basoni/loan_process/cmd        0.101s  coverage: 90.5% of statements
?       gitlab.com/fajar.basoni/loan_process/common     [no test files]
=== RUN   TestConnectSuccess
--- PASS: TestConnectSuccess (0.00s)
=== RUN   TestInitDatabaseSuccess
--- PASS: TestInitDatabaseSuccess (0.04s)
PASS
coverage: 100.0% of statements
ok      gitlab.com/fajar.basoni/loan_process/config     0.042s  coverage: 100.0% of statements
=== RUN   TestGetCustomerByNikSuccess
--- PASS: TestGetCustomerByNikSuccess (0.05s)
=== RUN   TestGetAgeSuccess
--- PASS: TestGetAgeSuccess (0.00s)
=== RUN   TestAddLoanSuccess
--- PASS: TestAddLoanSuccess (0.06s)
=== RUN   TestGetLoanSuccess
--- PASS: TestGetLoanSuccess (0.00s)
=== RUN   TestGetProvinceSuccess
--- PASS: TestGetProvinceSuccess (0.00s)
=== RUN   TestCreateRequestSuccess
--- PASS: TestCreateRequestSuccess (0.01s)
=== RUN   TestUpdateMaxRequestSuccess
--- PASS: TestUpdateMaxRequestSuccess (0.01s)
=== RUN   TestAddRequestSuccess
--- PASS: TestAddRequestSuccess (0.01s)
=== RUN   TestGetRequestSuccess
--- PASS: TestGetRequestSuccess (0.00s)
=== RUN   TestGetRequestByAmountSuccess
--- PASS: TestGetRequestByAmountSuccess (0.00s)
=== RUN   TestGetMasterSuccess
--- PASS: TestGetMasterSuccess (0.00s)
=== RUN   TestGetLastRequestSuccess
--- PASS: TestGetLastRequestSuccess (0.00s)
PASS
coverage: 84.2% of statements
ok      gitlab.com/fajar.basoni/loan_process/model      0.150s  coverage: 84.2% of statements
```

## Structure Project
    .
    ├── ...
    ├── loan_process                    
    │   ├── cmd
    │   |   ├── customer.go
    |   |   ├── installment.go 
    │   |   ├── province.go
    |   |   ├── request.go
    │   |   ├── customer_test.go
    |   |   ├── installment_test.go 
    │   |   ├── province_test.go
    |   |   ├── request_test.go            
    │   ├── common
    │   |   ├── common.go             
    │   ├── config
    │   |   ├── config.go      
    |   │   ├── config_test.go          
    │   ├── data
    │   |   ├── stored.db                 
    │   ├── model
    │   |   ├── customer.go
    |   |   ├── loan.go 
    │   |   ├── province.go
    |   |   ├── request.go
    |   |   ├── customer_test.go
    |   |   ├── loan_test.go 
    |   |   ├── province_test.go
    |   |   ├── request_test.go
    │   ├── .gitignore              
    │   ├── main.go             
    │   ├── README.md                          
    └── ...
