package cmd

import (
	"time"

	"gitlab.com/fajar.basoni/loan_process/common"
	"gitlab.com/fajar.basoni/loan_process/config"
	"gitlab.com/fajar.basoni/loan_process/model"
)

type Installment struct {
	IdLoan            string
	Month             int64
	DueDate           string
	AdministrationFee int64
	Capital           int64
	Total             int64
}

func ListInstallment(id string, tenor int64) ([]Installment, error) {
	result := []Installment{}
	database, err := config.Connect()
	common.CheckError(err)
	loan, _ := model.GetLoan(database, id)
	capital := int64(loan.Amount / tenor)
	today := time.Now()
	for i := 0; i < int(tenor); i++ {
		dueDate := today.AddDate(0, i, 0)
		strDueDate := dueDate.Format("02012006")
		installment := Installment{
			IdLoan:            loan.Id,
			Month:             int64(i + 1),
			DueDate:           strDueDate,
			AdministrationFee: loan.AdministrationFee,
			Capital:           capital,
			Total:             loan.AdministrationFee + capital,
		}
		result = append(result, installment)
	}
	return result, err
}
