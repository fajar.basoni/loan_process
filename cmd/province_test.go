package cmd

import (
	"testing"
)

func TestImportDataSuccess(t *testing.T) {
	expect := "COMMIT"
	actual, _ := ImportData()
	if actual != expect && actual != "ROLLBACK" {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}

func TestAssignProvinceSuccess(t *testing.T) {
	_, actual := AssignProvince()
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}
