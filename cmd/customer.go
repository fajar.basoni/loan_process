package cmd

import (
	"gitlab.com/fajar.basoni/loan_process/common"
	"gitlab.com/fajar.basoni/loan_process/config"
	"gitlab.com/fajar.basoni/loan_process/model"
)

func CustomerValidation(nik string) bool {
	result := true
	database, err := config.Connect()
	common.CheckError(err)
	customer, _ := model.GetCustomerByNik(database, nik)
	if customer.Age < 17 || customer.Age > 80 {
		result = false
	}
	if !AllowProvince(customer.Province.Code) {
		result = false
	}
	return result
}

func AllowProvince(code string) bool {
	result := false
	allowProvince := [4]string{"31", "32", "35", "12"}
	for i := 0; i < len(allowProvince); i++ {
		if code == allowProvince[i] {
			result = true
			break
		}
	}
	return result
}
