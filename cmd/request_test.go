package cmd

import (
	"testing"
	"time"

	"gitlab.com/fajar.basoni/loan_process/model"
)

func TestMultipleValidationSuccess(t *testing.T) {
	expect := true
	actual := MultipleValidation(1000000)
	if expect != actual {
		t.Errorf("Expected %v but got %v", expect, actual)
	}
}

func TestGenerationIdSuccess(t *testing.T) {
	expect := time.Now().Format("010206") + "01"
	actual := GenerateId()
	if expect != actual {
		t.Errorf("Expected %v but got %v", expect, actual)
	}
}

func TestCreateMaxDaySuccess(t *testing.T) {
	params := "50"
	actual := CreateMaxDay(params)
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}

func TestAddRequestSuccess(t *testing.T) {
	params := model.Request{
		Id:       GenerateId(),
		Nik:      "3201021603970008",
		FullName: "Test",
		Amount:   0,
		Status:   "Test",
	}
	expect := true
	actual, _ := AddRequest(params)
	if expect != actual.Success {
		t.Errorf("Expected %v but got %v", expect, actual)
	}
}

func TestRequestValidationSuccess(t *testing.T) {
	expect := true
	actual := RequestValidation()
	if expect != actual {
		t.Errorf("Expected %v but got %v", expect, actual)
	}
}

func TestGetRequestIdSuccess(t *testing.T) {
	params := time.Now().Format("010206") + "01"
	expect := true
	actual, _ := GetRequest(params)
	if expect != actual.Success {
		t.Errorf("Expected %v but got %v", expect, actual)
	}
}

func TestGetRequestByAmountSuccess(t *testing.T) {
	amount := 1000000
	status := "ACCEPTED"
	expect := true
	actual, _ := GetRequestByAmount(int64(amount), status)
	if expect != actual.Success {
		t.Errorf("Expected %v but got %v", expect, actual)
	}
}
