package cmd

import (
	"fmt"
	"strings"

	"gitlab.com/fajar.basoni/loan_process/common"
	"gitlab.com/fajar.basoni/loan_process/config"
	"gitlab.com/fajar.basoni/loan_process/model"
)

func ImportData() (string, error) {
	database, err := config.Connect()
	common.CheckError(err)
	provinces, _ := AssignProvince()
	valueStrings := []string{}
	valueArgs := []interface{}{}
	for _, w := range provinces {
		valueStrings = append(valueStrings, "(?, ?)")

		valueArgs = append(valueArgs, w.Code)
		valueArgs = append(valueArgs, w.Name)
	}
	statement := `INSERT INTO province(Code, Name) VALUES %s`
	statement = fmt.Sprintf(statement, strings.Join(valueStrings, ","))
	transaction, _ := database.Begin()
	_, err = transaction.Exec(statement, valueArgs...)
	if err != nil {
		return "ROLLBACK", transaction.Rollback()
	}
	return "COMMIT", transaction.Commit()
}

func AssignProvince() ([]model.Province, error) {
	provinces := []model.Province{}
	provinces = append(provinces, model.Province{Code: "11", Name: "Aceh"})
	provinces = append(provinces, model.Province{Code: "12", Name: "Sumatera Utara"})
	provinces = append(provinces, model.Province{Code: "13", Name: "Sumatera Barat"})
	provinces = append(provinces, model.Province{Code: "14", Name: "Riau"})
	provinces = append(provinces, model.Province{Code: "21", Name: "Kepulauan Riau"})
	provinces = append(provinces, model.Province{Code: "15", Name: "Jambi"})
	provinces = append(provinces, model.Province{Code: "16", Name: "Sumatera Selatan"})
	provinces = append(provinces, model.Province{Code: "19", Name: "Kepulauan Bangka Belitung"})
	provinces = append(provinces, model.Province{Code: "17", Name: "Bengkulu"})
	provinces = append(provinces, model.Province{Code: "18", Name: "Lampung"})
	provinces = append(provinces, model.Province{Code: "31", Name: "DKI Jakarta"})
	provinces = append(provinces, model.Province{Code: "36", Name: "Banten"})
	provinces = append(provinces, model.Province{Code: "32", Name: "Jawa Barat"})
	provinces = append(provinces, model.Province{Code: "33", Name: "Jawa Tengah"})
	provinces = append(provinces, model.Province{Code: "34", Name: "DI Yogyakarta"})
	provinces = append(provinces, model.Province{Code: "35", Name: "Jawa Timur"})
	provinces = append(provinces, model.Province{Code: "51", Name: "Bali"})
	provinces = append(provinces, model.Province{Code: "52", Name: "Nusa Tenggara Barat"})
	provinces = append(provinces, model.Province{Code: "53", Name: "Nusa Tenggara Timur"})
	provinces = append(provinces, model.Province{Code: "61", Name: "Kalimantan Barat"})
	provinces = append(provinces, model.Province{Code: "62", Name: "Kalimantan Tengah"})
	provinces = append(provinces, model.Province{Code: "63", Name: "Kalimantan Selatan"})
	provinces = append(provinces, model.Province{Code: "64", Name: "Kalimantan Timur"})
	provinces = append(provinces, model.Province{Code: "65", Name: "Kalimantan Utara"})
	provinces = append(provinces, model.Province{Code: "71", Name: "Sulawesi Utara"})
	provinces = append(provinces, model.Province{Code: "75", Name: "Gorontalo"})
	provinces = append(provinces, model.Province{Code: "72", Name: "Sulawesi Tengah"})
	provinces = append(provinces, model.Province{Code: "76", Name: "Sulawesi Barat"})
	provinces = append(provinces, model.Province{Code: "73", Name: "Sulawesi Selatan"})
	provinces = append(provinces, model.Province{Code: "74", Name: "Sulawesi Tenggara"})
	provinces = append(provinces, model.Province{Code: "81", Name: "Maluku"})
	provinces = append(provinces, model.Province{Code: "82", Name: "Maluku Utara"})
	provinces = append(provinces, model.Province{Code: "92", Name: "Papua Barat"})
	provinces = append(provinces, model.Province{Code: "91", Name: "Papua"})
	return provinces, nil
}
