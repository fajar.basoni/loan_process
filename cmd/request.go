package cmd

import (
	"strconv"
	"strings"
	"time"

	"gitlab.com/fajar.basoni/loan_process/common"
	"gitlab.com/fajar.basoni/loan_process/config"
	"gitlab.com/fajar.basoni/loan_process/model"
)

func MultipleValidation(amount int64) bool {
	if amount%1000000 != 0 {
		return false
	}
	if amount < 1000000 {
		return false
	}
	if amount > 10000000 {
		return false
	}
	return true
}

func GenerateId() string {
	database, err := config.Connect()
	common.CheckError(err)
	today := time.Now().Format("010206")
	request, _ := model.GetLastRequest(database, today)
	if request.Id != "" {
		lastId, _ := strconv.ParseInt(request.Id[6:8], 10, 64)
		currentId := strconv.Itoa(int(lastId + 1))
		return request.Id[0:7] + currentId
	}
	return today + "01"
}

func RequestValidation() bool {
	database, err := config.Connect()
	common.CheckError(err)
	today := time.Now().Format("010206")
	master, _ := model.GetMaster(database)
	if master.Date != today || master.Max <= 0 {
		return false
	}
	return true
}

func CreateMaxDay(params string) error {
	database, err := config.Connect()
	common.CheckError(err)
	max, _ := strconv.Atoi(params)
	today := time.Now().Format("010206")
	model.CreateRequest(database, max, today)
	return err
}

func AddRequest(params model.Request) (common.Result, model.Request) {
	result := common.Result{}
	database, err := config.Connect()
	common.CheckError(err)
	customer, _ := model.GetCustomerByNik(database, params.Nik)
	loan := model.Loan{}
	if RequestValidation() && CustomerValidation(params.Nik) && MultipleValidation(params.Amount) {
		params.Status = "ACCEPTED"
		loan = model.Loan{
			Id:                params.Id,
			Nik:               params.Nik,
			FullName:          params.FullName,
			Gender:            customer.Gender,
			AdministrationFee: 100000, // wiil change with parameterize
			Amount:            params.Amount,
		}
		model.AddLoan(database, loan)
	} else {
		params.Status = "REJECTED"
	}

	result.Success = model.AddRequest(database, params)
	result.Message = "Failed"
	if result.Success {
		result.Message = "Success"
	}
	model.UpdateMaxRequest(database)
	return result, params
}

func GetRequest(id string) (common.Result, model.Request) {
	database, err := config.Connect()
	common.CheckError(err)
	result, _ := model.GetRequest(database, id)

	return common.Result{
		Success: true,
		Message: "Success",
	}, result
}

func GetRequestByAmount(amount int64, status string) (common.Result, string) {
	database, err := config.Connect()
	common.CheckError(err)
	result := []string{}
	list, _ := model.GetRequestByAmount(database, amount, status)
	for e := list.Front(); e != nil; e = e.Next() {
		item := string(e.Value.(string))
		result = append(result, item)
	}
	return common.Result{
		Success: true,
		Message: "Success",
	}, strings.Join(result[:], " ")
}
