package cmd

import (
	"testing"

	"gitlab.com/fajar.basoni/loan_process/config"
)

func TestInitDatabaseSuccess(t *testing.T) {
	actual := config.InitDatabase()
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}

func TestImportDataInitSuccess(t *testing.T) {
	expect := "COMMIT"
	actual, _ := ImportData()
	if actual != expect && actual != "ROLLBACK" {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}

func TestCustomerValidationSuccess(t *testing.T) {
	expect := true
	actual := CustomerValidation("3201021503970008")
	if actual != expect {
		t.Errorf("Expected %v but got %v", expect, actual)
	}
}

func TestAllowProvinceSuccess(t *testing.T) {
	expect := true
	actual := AllowProvince("32")
	if actual != expect {
		t.Errorf("Expected %v but got %v", expect, actual)
	}
}
