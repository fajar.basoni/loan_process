package cmd

import (
	"testing"
	"time"
)

func TestListInstallmentSuccess(t *testing.T) {
	today := time.Now().Format("010206") + "01"
	_, actual := ListInstallment(today, int64(3))
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}
