package model

import (
	"testing"

	"gitlab.com/fajar.basoni/loan_process/config"
)

func TestGetProvinceSuccess(t *testing.T) {
	database, _ := config.Connect()
	params := "32"
	_, actual := GetProvince(database, params)
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}
