package model

import (
	"testing"
	"time"

	"gitlab.com/fajar.basoni/loan_process/config"
)

func TestGetCustomerByNikSuccess(t *testing.T) {
	database, _ := config.Connect()
	_, actual := GetCustomerByNik(database, "3201021503970008")
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}

func TestGetAgeSuccess(t *testing.T) {
	birthDate, _ := time.Parse("01/02/2006", "03/15/1997")
	todayDate, _ := time.Parse("01/02/2006", time.Now().Format("01/02/2006"))
	expect := 24
	actual := GetAge(birthDate, todayDate)
	if actual != expect {
		t.Errorf("Expected %v but got %v", expect, actual)
	}
}
