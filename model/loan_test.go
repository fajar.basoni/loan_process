package model

import (
	"testing"

	"gitlab.com/fajar.basoni/loan_process/config"
)

func TestAddLoanSuccess(t *testing.T) {
	database, _ := config.Connect()
	params := Loan{
		Id:                "99999999",
		Nik:               "Test",
		FullName:          "Test",
		Gender:            "Test",
		AdministrationFee: 0,
		Amount:            0,
	}
	expect := true
	actual := AddLoan(database, params)
	if actual != expect {
		t.Errorf("Expected %v but got %v", expect, actual)
	}
}

func TestGetLoanSuccess(t *testing.T) {
	database, _ := config.Connect()
	params := "04182101"
	_, actual := GetLoan(database, params)
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}
