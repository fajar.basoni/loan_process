package model

import (
	"database/sql"

	"gitlab.com/fajar.basoni/loan_process/common"
)

type Loan struct {
	Id                string
	Nik               string
	FullName          string
	Gender            string
	AdministrationFee int64
	Amount            int64
}

func AddLoan(db *sql.DB, params Loan) bool {
	result := false
	transaction, err := db.Begin()
	common.CheckError(err)
	statement, err := transaction.Prepare("INSERT INTO loan (Id, Nik, FullName, Gender, AdministrationFee) VALUES (?,?,?,?,?)")
	common.CheckError(err)
	defer statement.Close()
	sqlResult, err := statement.Exec(params.Id, params.Nik, params.FullName, params.Gender, params.AdministrationFee)
	common.CheckError(err)
	id, _ := sqlResult.LastInsertId()
	if id > 0 {
		result = true
		transaction.Commit()
	} else {
		transaction.Rollback()
	}
	return result
}

func GetLoan(db *sql.DB, Id string) (Loan, error) {
	rows, err := db.Query("select l.*, r.Amount from loan as l inner join request as r on l.Id = r.Id where l.Id = '" + Id + "'")
	common.CheckError(err)
	defer rows.Close()
	for rows.Next() {
		var loan Loan
		err = rows.Scan(&loan.Id, &loan.Nik, &loan.FullName, &loan.Gender, &loan.AdministrationFee, &loan.Amount)
		common.CheckError(err)
		return loan, err
	}
	defer db.Close()
	return Loan{}, err
}
