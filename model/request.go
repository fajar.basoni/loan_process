package model

import (
	"container/list"
	"database/sql"
	"strconv"

	"gitlab.com/fajar.basoni/loan_process/common"
)

type Master struct {
	Max  int
	Date string
}

type Request struct {
	Id       string
	Nik      string
	FullName string
	Amount   int64
	Status   string
}

func CreateRequest(db *sql.DB, max int, date string) error {
	transaction, err := db.Begin()
	common.CheckError(err)
	statement, err := transaction.Prepare("UPDATE master SET Max = ?, Date = ? WHERE Code = 'REQUEST'")
	common.CheckError(err)
	defer statement.Close()
	_, err = statement.Exec(max, date)
	common.CheckError(err)
	transaction.Commit()
	return err
}

func UpdateMaxRequest(db *sql.DB) error {
	transaction, err := db.Begin()
	common.CheckError(err)
	statement, err := transaction.Prepare("UPDATE master SET Max = Max - 1 WHERE Code = 'REQUEST'")
	common.CheckError(err)
	defer statement.Close()
	_, err = statement.Exec()
	common.CheckError(err)
	transaction.Commit()
	return err
}

func AddRequest(db *sql.DB, params Request) bool {
	result := false
	transaction, err := db.Begin()
	common.CheckError(err)
	statement, err := transaction.Prepare("INSERT INTO request (Id, Nik, FullName, Amount, Status) VALUES (?,?,?,?,?)")
	common.CheckError(err)
	defer statement.Close()
	sqlResult, err := statement.Exec(params.Id, params.Nik, params.FullName, params.Amount, params.Status)
	common.CheckError(err)
	id, _ := sqlResult.LastInsertId()
	if id > 0 {
		result = true
		transaction.Commit()
	} else {
		transaction.Rollback()
	}
	return result
}

func GetRequest(db *sql.DB, id string) (Request, error) {
	rows, err := db.Query("select * from request where id = '" + id + "'")
	common.CheckError(err)
	defer rows.Close()
	for rows.Next() {
		var request Request
		err = rows.Scan(&request.Id, &request.Nik, &request.FullName, &request.Amount, &request.Status)
		common.CheckError(err)
		return request, err
	}
	defer db.Close()
	return Request{}, err
}

func GetRequestByAmount(db *sql.DB, amount int64, status string) (list.List, error) {
	list := list.New()
	rows, err := db.Query("select * from request where amount = " + strconv.Itoa(int(amount)) + " and status = '" + status + "'")
	common.CheckError(err)
	defer rows.Close()
	for rows.Next() {
		var request Request
		err = rows.Scan(&request.Id, &request.Nik, &request.FullName, &request.Amount, &request.Status)
		common.CheckError(err)
		list.PushBack(request.Id)
	}
	defer db.Close()
	return *list, err
}

func GetMaster(db *sql.DB) (Master, error) {
	rows, err := db.Query("select Max, Date from master where Code = 'REQUEST'")
	common.CheckError(err)
	defer rows.Close()
	for rows.Next() {
		var master Master
		err = rows.Scan(&master.Max, &master.Date)
		common.CheckError(err)
		return master, err
	}
	defer db.Close()
	return Master{}, err
}

func GetLastRequest(db *sql.DB, date string) (Request, error) {
	rows, err := db.Query("select * from request where substr(Id, 1, 6) = '" + date + "' order by Id desc limit 1;")
	common.CheckError(err)
	defer rows.Close()
	for rows.Next() {
		var request Request
		err = rows.Scan(&request.Id, &request.Nik, &request.FullName, &request.Amount, &request.Status)
		common.CheckError(err)
		return request, err
	}
	defer db.Close()
	return Request{}, err
}
