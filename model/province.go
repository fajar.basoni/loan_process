package model

import (
	"database/sql"

	"gitlab.com/fajar.basoni/loan_process/common"
)

type Province struct {
	Code string
	Name string
}

func GetProvince(db *sql.DB, code string) (Province, error) {
	rows, err := db.Query("select * from province where Code = '" + code + "'")
	common.CheckError(err)
	defer rows.Close()
	for rows.Next() {
		var province Province
		err = rows.Scan(&province.Code, &province.Name)
		common.CheckError(err)
		return province, err
	}
	defer db.Close()
	return Province{}, err
}
