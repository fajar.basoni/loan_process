package model

import (
	"testing"
	"time"

	"gitlab.com/fajar.basoni/loan_process/config"
)

func TestCreateRequestSuccess(t *testing.T) {
	database, _ := config.Connect()
	max := 3
	date := time.Now().Format("010206")
	actual := CreateRequest(database, max, date)
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}

func TestUpdateMaxRequestSuccess(t *testing.T) {
	database, _ := config.Connect()
	actual := UpdateMaxRequest(database)
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}

func TestAddRequestSuccess(t *testing.T) {
	database, _ := config.Connect()
	params := Request{
		Id:       "99999999",
		Nik:      "Test",
		FullName: "Test",
		Amount:   0,
		Status:   "Test",
	}
	expect := true
	actual := AddRequest(database, params)
	if actual != expect {
		t.Errorf("Expected %v but got %v", expect, actual)
	}
}

func TestGetRequestSuccess(t *testing.T) {
	database, _ := config.Connect()
	params := time.Now().Format("010206") + "01"
	_, actual := GetRequest(database, params)
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}

func TestGetRequestByAmountSuccess(t *testing.T) {
	database, _ := config.Connect()
	amount := 1000000
	status := "ACCEPTED"
	_, actual := GetRequestByAmount(database, int64(amount), status)
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}

func TestGetMasterSuccess(t *testing.T) {
	database, _ := config.Connect()
	_, actual := GetMaster(database)
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}

func TestGetLastRequestSuccess(t *testing.T) {
	database, _ := config.Connect()
	params := time.Now().Format("010206")
	_, actual := GetLastRequest(database, params)
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}
