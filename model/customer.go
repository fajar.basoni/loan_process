package model

import (
	"database/sql"
	"strconv"
	"time"
)

type Customer struct {
	Nik      string
	FullName string
	Age      int
	Gender   string
	Province Province
}

func GetCustomerByNik(db *sql.DB, nik string) (Customer, error) {
	codeProvince := nik[0:2]
	DD := nik[6:8]
	MM := nik[8:10]
	YY := nik[10:12]

	province, _ := GetProvince(db, codeProvince)

	bornDay, _ := strconv.ParseInt(DD, 10, 64)
	gender := "MALE"
	if bornDay >= 40 {
		gender = "FEMALE"
		DD = strconv.Itoa(int(bornDay - 40))
		if len(DD) == 1 {
			DD = "0" + DD
		}
	}

	layout := "01/02/2006"
	lastYY, _ := strconv.ParseInt(YY, 10, 64)
	YYYY := "19" + YY
	if lastYY < 21 { // will change with today year
		YYYY = "20" + YY
	}

	birthDate, _ := time.Parse(layout, MM+"/"+DD+"/"+YYYY)
	todayDate, _ := time.Parse(layout, time.Now().Format(layout))
	age := GetAge(birthDate, todayDate)

	customer := Customer{}
	customer.Nik = nik
	customer.FullName = ""
	customer.Age = age
	customer.Gender = gender
	customer.Province = province

	return customer, nil
}

func GetAge(birthdate, today time.Time) int {
	today = today.In(birthdate.Location())
	ty, tm, td := today.Date()
	today = time.Date(ty, tm, td, 0, 0, 0, 0, time.UTC)
	by, bm, bd := birthdate.Date()
	birthdate = time.Date(by, bm, bd, 0, 0, 0, 0, time.UTC)
	if today.Before(birthdate) {
		return 0
	}
	age := ty - by
	anniversary := birthdate.AddDate(age, 0, 0)
	if anniversary.After(today) {
		age--
	}
	return age
}
