package config

import "testing"

func TestConnectSuccess(t *testing.T) {
	_, actual := Connect()
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}

func TestInitDatabaseSuccess(t *testing.T) {
	actual := InitDatabase()
	if actual != nil {
		t.Errorf("Expected %v but got %v", nil, actual)
	}
}
