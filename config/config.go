package config

import (
	"database/sql"
	"os"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/fajar.basoni/loan_process/common"
)

func Connect() (*sql.DB, error) {
	database, err := sql.Open("sqlite3", os.Getenv("PROJECTPATH")+"/data/stored.db")
	common.CheckError(err)
	return database, err
}

func InitDatabase() error {
	database, err := Connect()
	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS master (Code TEXT PRIMARY KEY, Max INTEGER, Date TEXT)")
	statement.Exec()
	statement, _ = database.Prepare("CREATE TABLE IF NOT EXISTS province (Code TEXT PRIMARY KEY, Name TEXT)")
	statement.Exec()
	statement, _ = database.Prepare("CREATE TABLE IF NOT EXISTS request (Id TEXT PRIMARY KEY, Nik TEXT, FullName TEXT, Amount NUMERIC, Status TEXT)")
	statement.Exec()
	statement, _ = database.Prepare("CREATE TABLE IF NOT EXISTS loan (Id TEXT PRIMARY KEY, Nik TEXT, FullName TEXT, Gender TEXT, AdministrationFee TEXT)")
	statement.Exec()
	statement, _ = database.Prepare("INSERT INTO master(Code,Max,Date) SELECT 'REQUEST', 0, '' WHERE NOT EXISTS(SELECT 1 FROM master WHERE Code = 'REQUEST');")
	statement.Exec()
	database.Close()
	return err
}
